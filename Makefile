# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GORUN=$(GOCMD) run
GOFMT=gofmt -w


SRC = src/*.go
BIN = bin

all: build
build:
	$(GOFMT) $(SRC)
	for os in windows linux darwin ; do \
    GOOS=$$os $(GOBUILD) -o $(BIN)/hello_world_$$os $(SRC) ; \
	done
	mv $(BIN)/hello_world_windows $(BIN)/hello_world_windows.exe
run:
	$(GORUN) $(SRC)

fmt:
	$(GOFMT) $(SRC)
